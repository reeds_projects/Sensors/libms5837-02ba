
## LICENCE

Cecill licensed code

Company: REEDS

Author: ROPARS Benoît (benoit.ropars@solutionsreeds.fr)

All codes are made available free of charge. These are codes from the manufacturer's documentation.
Libraries using this code are available under a proprietary license.We can also develop specific solutions for your needs. You can contact us via the following address: contact@solutionsreeds.fr

## CONAN PACKAGE

[![pipeline status](https://gitlab.com/reeds_projects/Sensors/libms5837-02ba/badges/master/pipeline.svg)](https://gitlab.com/reeds_projects/Sensors/libms5837-02ba/-/commits/master)


## For Users

### Basic setup

    $ conan install ms5837-02ba/1.0@/ --remote=gitlab

### Project setup

If you handle multiple dependencies in your project is better to add a *conanfile.txt*

    [requires]
    ms5837-02ba/1.0@reeds_projects+Sensors+libms5837-02ba/stable

    [generators]
    cmake

Complete the installation of requirements for your project running:

    $ mkdir build && cd build && conan install ..

Note: It is recommended that you run conan install from a build directory and not the root of the project directory.  This is because conan generates *conanbuildinfo* files specific to a single build configuration which by default comes from an autodetected default profile located in ~/.conan/profiles/default .  If you pass different build configuration options to conan install, it will generate different *conanbuildinfo* files.  Thus, they should not be added to the root of the project, nor committed to git.


## Build and package

The following command both runs all the steps of the conan file, and publishes the package to the local system cache.  This includes downloading dependencies from "build_requires" and "requires" , and then running the build() method.

    $ conan create . reeds_projects+Sensors+libms5837-02ba/stable


## Test package

    $ conan test apps libms5837-02ba/1.0@reeds_projects+Sensors+libms5837-02ba/stable


### Available Options
| Option        | Default | Possible Values  |
| ------------- |:----------------- |:------------:|
| shared      | False |  [True, False] |



## Add Remote

Conan Community has its own Bintray repository, however, we are working to distribute all package in the Conan Center:

    $ conan remote add gitlab https://gitlab.com/api/v4/packages/conan



## MODIFICATION

- v.1.0 (30/06/2020):
	- First version

## TROUBLESHOTING



## TODO
