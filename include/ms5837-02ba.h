
#pragma once

#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>

#include <mraa/i2c.h>
#include <mraa/spi.h>
#include <mraa/gpio.h>

#include "ms5837-02ba_defs.h"

#ifdef __cplusplus
extern "C" {
#endif

    /**
     * @file ms5837_02ba
     * @library ms5837_02ba
     * @brief C API for the MS5837_02ba Pressure and Temperature sensor
     *
     * @include ms5837_02ba.c
     */

    /**
     * Device context
     */
    typedef struct _ms5837_02ba_context {
        mraa_i2c_context        i2c;
        mraa_spi_context        spi;
        // CS pin, if we are using one
        mraa_gpio_context       gpio;

        // whether we are doing I2C or SPI
        int8_t                    isSPI;

        // stored calibration coefficients
        uint16_t                C[MS5837_02ba_MAX_COEFFICIENTS];

        // the command sent to chip depending on OSR configuration for
        // temperature and pressure measurement.
        MS5837_02ba_CMD_T            temperatureCmd;
        MS5837_02ba_OSR_T            temperatureDelay;

        MS5837_02ba_CMD_T            pressureCmd;
        MS5837_02ba_OSR_T            pressureDelay;

        // compensated temperature in C
        float                   temperature;
        // compensated pressure in millibars
        float                   pressure;
        //depth offset
        float                   offset;
    } *ms5837_02ba_context;

    /**
     * MS5837_02ba initializer
     *
     * @param bus i2c/spi bus to use
     * @param address The address for this device if using I2C.  If
     * using SPI, supply -1 for this parameter.
     * @param cs_pin The GPIO pin to use for Chip Select (CS).  This is
     * only needed for SPI, and only if your SPI implementation
     * requires it.  Otherwise, just pass -1 if not using SPI, or your
     * CS is handled automatically by your SPI implementation.
     * @return an initialized device context on success, NULL on error.
     */
    ms5837_02ba_context ms5837_02ba_init(unsigned int bus, int address, int cs_pin);

    /**
     * MS5837_02ba close
     *
     * @param dev Device context.
     */
    void ms5837_02ba_close(ms5837_02ba_context dev);

    /**
     * Reset the device.
     *
     * @param dev Device context.
     * @return Status.
     */
    int16_t ms5837_02ba_reset(const ms5837_02ba_context dev);

    /**
     * Take measurements and store the current sensor values
     * internally.  This function must be called prior to retrieving
     * any sensor values, for example ms5837_02ba_get_temperature().
     *
     * @param dev Device context.
     */
    int16_t ms5837_02ba_update(const ms5837_02ba_context dev);

    /**
     * Set the depth offset
     *
     * @param dev Device context.
     * @param offset in meter.
     */
    void ms5837_02ba_set_depth_offset(const ms5837_02ba_context dev,float offset);

    /**
     * Set the output sampling resolution of the temperature
     * measurement.  Higher values provide a more precise value.  In
     * addition, more precise values require more time to measure.
     * The default set at device intialization is the highest
     * precision supported: MS5837_02ba_OSR_4096
     *
     * @param dev Device context.
     * @param osr One of the MS5837_02ba_OSR_T values.
     */
    void ms5837_02ba_set_temperature_osr(const ms5837_02ba_context dev,
                                    MS5837_02ba_OSR_T osr);

    /**
     * Set the output sampling resolution of the pressure
     * measurement.  Higher values provide a more precise value.  In
     * addition, more precise values require more time to measure.
     * The default set at device intialization is the highest
     * precision supported: MS5837_02ba_OSR_4096
     *
     * @param dev Device context.
     * @param osr One of the MS5837_02ba_OSR_T values.
     */
    void ms5837_02ba_set_pressure_osr(const ms5837_02ba_context dev,
                                 MS5837_02ba_OSR_T osr);

    /**
     * Return the latest measured temperature.  ms5837_02ba_update() must
     * have been called prior to calling this function.  The returned
     * value is in degrees Celsius.
     *
     * @param dev Device context.
     * @return Temperature in degrees C
     */
    float ms5837_02ba_get_temperature(const ms5837_02ba_context dev);

    /**
     * Return the latest measured pressure.  ms5837_02ba_update() must have
     * been called prior to calling this function.  The returned value
     * is in millibars.
     *
     * @param dev Device context.
     * @return Pressure in mbar
     */
    float ms5837_02ba_get_pressure(const ms5837_02ba_context dev);

    /**
     * Return the latest measured depth.  ms5837_02ba_update() must have
     * been called prior to calling this function.  The returned value
     * is in m.
     *
     * @param dev Device context.
     * @return Pressure in m
     */
    float ms5837_02ba_get_depth(const ms5837_02ba_context dev);

    /**
     * Load a series of factory installed compensation coefficients.
     * This function is called during ms5837_02ba_init(), so it should
     * never need to be called again.  It is provided here anyway
     * "just in case".
     *
     * @param dev Device context.
     * @return Status.
     */
    int16_t ms5837_02ba_load_coefficients(const ms5837_02ba_context dev);

    /**
     * Perform a bus read.  This function is bus agnostic.  It is
     * exposed here for those users wishing to perform their own low
     * level accesses.  This is a low level function, and should not
     * be used unless you know what you are doing.
     *
     * @param dev Device context
     * @param cmd The command to send.
     * @param data A pointer to a buffer in which data will be read into.
     * @param len The number of bytes to read.
     * @return Status
     */
    int16_t ms5837_02ba_bus_read(const ms5837_02ba_context dev, uint8_t cmd,
                                 uint8_t *data, uint8_t len);

    /**
     * Perform a bus write.  This function is bus agnostic.  It is
     * exposed here for those users wishing to perform their own low
     * level accesses.  This is a low level function, and should not
     * be used unless you know what you are doing.
     *
     * @param dev Device context
     * @param cmd The command to send.
     * @param data A pointer to a buffer containing data to write.
     * @param len The number of bytes to write.
     * @return Status
     */
    int16_t ms5837_02ba_bus_write(const ms5837_02ba_context dev, uint8_t cmd,
                                  uint8_t *data, uint8_t len);

#ifdef __cplusplus
}
#endif
