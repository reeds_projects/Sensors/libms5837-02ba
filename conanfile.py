from conans import ConanFile, CMake, tools, load
import os


class templateConan(ConanFile):
    # -------- User variables-----------
    project_name = "ms5837-02ba"
    project_type = "lib"  #lib or app
    version = "1.0"
    license = "CECILL"
    author = "Benoit ROPARS (benoit.ropars@solutionsreeds.fr)"
    requires=["mraa/2.1@reeds_projects+conan_extern_pkg+conan_mraa/stable"]
    #-----------------------------------

    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False]}
    default_options = {"shared": False}
    generators = "cmake"
    exports_sources = ["src/*","include/*","apps/*","tests/*","CMakeLists.txt"]
    name = "{}{}".format(project_type, project_name)

    def build(self):
        cmake = CMake(self)
        cmake.definitions["CMAKE_PROJECT_NAME"] = self.project_name
        cmake.configure(source_folder="src")
        cmake.build()

    def package(self):
        self.copy("*", src="bin", dst="bin")
        self.copy("*.h", dst="include", src= "include")
        self.copy("*.lib", dst="lib", keep_path=False)
        self.copy("*.dll", dst="bin", keep_path=False)
        self.copy("*.so", dst="lib", keep_path=False)
        self.copy("*.dylib", dst="lib", keep_path=False)
        self.copy("*.a", dst="lib", keep_path=False)

    def package_info(self):
        self.cpp_info.libs = [self.project_name]
