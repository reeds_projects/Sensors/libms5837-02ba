#include <unistd.h>
#include <stdio.h>
#include <signal.h>

#include "ms5837-02ba.h"

int8_t shouldRun = 1;

void sig_handler(int signo)
{
  if (signo == SIGINT)
    shouldRun = 0;
}

int main()
{
  signal(SIGINT, sig_handler);

  // Instantiate a MS5837_02ba on analog I2C bus 0, at the default address
  ms5837_02ba_context sensor = ms5837_02ba_init(0, MS5837_02ba_DEFAULT_I2C_ADDR, -1);

  if (!sensor)
  {
      printf("ms5837_02ba_init() failed\n");
      return 1;
  }

  // Every second, sample the sensor and output the pressure and
  // temperature


  int8_t firstRun = 0;
  while (shouldRun)
    {
        if (ms5837_02ba_update(sensor))
        {
            printf("ms5837_02ba_update() failed\n");
        }

        if(firstRun == 0){
            firstRun = 1;
            float depth = ms5837_02ba_get_depth(sensor);
            ms5837_02ba_set_depth_offset(sensor,-depth);
        }

        printf("Temperature: %f C, Pressure = %f mbar, Depth = %f m \n",
               ms5837_02ba_get_temperature(sensor),
               ms5837_02ba_get_pressure(sensor),
               ms5837_02ba_get_depth(sensor));

        sleep(1);
    }


  printf("Exiting\n");

  ms5837_02ba_close(sensor);

  return 0;
}
